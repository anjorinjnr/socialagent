package com.socialagent.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.socialagent.data.ObjectifyConnectionRepository;
import com.socialagent.domain.FacebookAgent;
import com.socialagent.domain.FacebookPage;
import com.socialagent.domain.PageList;
import com.socialagent.service.AgentService;
import com.socialagent.service.ChannelService;
import com.socialagent.service.FacebookServiceImpl;
import com.socialagent.service.UserService;
import com.socialagent.utils.AppUtil;

@Controller
public class HomeController {

	UserService userService;
	ChannelService channelService;
	AgentService agentService;
	FacebookServiceImpl fbService;

	private static final Logger log = Logger.getLogger(HomeController.class
			.getName());

	@Autowired
	public HomeController(UserService userService,
			ChannelService channelService, AgentService agentService,
			FacebookServiceImpl fbService) {
		this.userService = userService;
		this.channelService = channelService;
		this.agentService = agentService;
		this.fbService = fbService;
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String index(Model model) {
		log.info("showing home page");
		if (AppUtil.currentUser() == null)
			return "redirect:/login";
		model.addAttribute("user", AppUtil.currentUser());
		return "home";
	}

	@RequestMapping(value = "/ajax/newagent", method = RequestMethod.GET)
	public String newAgentView(Model model) {
		model.addAttribute("channels", channelService.getSocialChannels());
		return "newagent";
	}

	@RequestMapping(value = "/ajax/channel/options/{channel}/{channelId}", method = RequestMethod.GET)
	public String channelOptions(@PathVariable String channel,
			@PathVariable String channelId, Model model) {

		/*
		 * List<FacebookPage> pgs = new ArrayList<>(); FacebookPage fb1 = new
		 * FacebookPage(); fb1.setAccess_token(
		 * "CAACEdEose0cBAMuOLtCG3n3i65H0sc3OBOf9P2KIZBrHWBQHww204L8HFXyF4lReWPMg4yXIFCDZCUe1nBhw4ttvX0N9f9blmWpJJRwBVYJjbZCHVGsJ7jJq0n44LrOWB4voTriTfVJaxJgndm07j9BtqPGE4a8EZBDBdL4cRGTGE3PMSVc72IvCxcSXTS4ZD"
		 * ); fb1.setCategory("Internet/software");
		 * fb1.setId("111850665561577"); fb1.setName("Adloopz"); pgs.add(fb1);
		 * FacebookPage fb2 = new FacebookPage(); fb2.setAccess_token(
		 * "CAACEdEose0cBAMuOLtCG3n3i65H0sc3OBOf9P2KIZBrHWBQHww204L8HFXyF4lReWPMg4yXIFCDZCUe1nBhw4ttvX0N9f9blmWpJJRwBVYJjbZCHVGsJ7jJq0n44LrOWB4voTriTfVJaxJgndm07j9BtqPGE4a8EZBDBdL4cRGTGE3PMSVc72IvCxcSXTS4ZD"
		 * ); fb2.setCategory("Consulting/business services");
		 * fb2.setId("274472322596421"); fb2.setName("HB Careers");
		 * pgs.add(fb2);
		 */

		// model.addAttribute("pageList", pgs);
		PageList fbPageList = fbService.getPages();
		model.addAttribute("pageList", fbPageList.getData());
		return "fbpages";
	}

	@RequestMapping(value = "/ajax/newagent", method = RequestMethod.POST)
	public @ResponseBody
	String createAgent(@RequestParam String data) {
		FacebookAgent fbAgent = null;
		ObjectMapper mapper = new ObjectMapper();

		try {
			fbAgent = mapper.readValue(data, FacebookAgent.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			log.info(e.toString());
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			log.info(e.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.info(e.toString());
		}

		// AppUtil.print(fbAgent);
		if (fbAgent != null) {
			fbAgent.setUserId(AppUtil.currentUser().getEmail());
			this.agentService.addAgent(fbAgent);
		}
		return "newagent";
	}


}
