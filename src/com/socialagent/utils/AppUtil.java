package com.socialagent.utils;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.socialagent.domain.User;

public class AppUtil {

	public static final String CURRENT_USER = "CurrentUser";
    public static User currentUser() {
        User user = (User) session().getAttribute(CURRENT_USER);
        return user;
    }
    
    public static HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true); // true == allow create
    }
    
    public static void print(Object object) {
        System.out.println(object);
    }

    public static boolean listIsNullOrEmpty(List<?> l){
    	return l == null || l.size() == 0;
    }
}
