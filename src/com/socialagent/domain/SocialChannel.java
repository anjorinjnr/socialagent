package com.socialagent.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;

@SuppressWarnings("serial")
@Entity
public class SocialChannel implements DomainObject, Serializable {


	private @Id
	Long id;
	
	private Integer version;
	//private Key<User> user;

	// this is the user's email
	private String userId;

	@Column(nullable = false)
	private String providerId;

	private String providerUserId;

	@Column(nullable = false)
	private int rank;

	private String displayName;

	private String profileUrl;

	private String imageUrl;

	@Column(nullable = false)
	private String accessToken;

	private String secret;

	private String refreshToken;
	private Long expireTime;

	private Date createDate = new Date();

	public SocialChannel() {
		super();
	}

	/*
	private void init(String providerId, String providerUserId, int rank,
			String displayName, String profileUrl, String imageUrl,
			String accessToken, String secret, String refreshToken,
			Long expireTime, Date createDate) {
		
		this.providerId = providerId;
		this.providerUserId = providerUserId;
		this.rank = rank;
		this.displayName = displayName;
		this.profileUrl = profileUrl;
		this.imageUrl = imageUrl;
		this.accessToken = accessToken;
		this.secret = secret;
		this.refreshToken = refreshToken;
		this.expireTime = expireTime;
		this.createDate = createDate;
	}

	public SocialAgent(String userId, String providerId, String providerUserId,
			int rank, String displayName, String profileUrl, String imageUrl,
			String accessToken, String secret, String refreshToken,
			Long expireTime, Date createDate) {

		super();
		this.user = user;

	} */

//	public SocialChannel(Key<User> user, String userId, String providerId,
//			String providerUserId, int rank, String displayName,
//			String profileUrl, String imageUrl, String accessToken,
//			String secret, String refreshToken, Long expireTime, Date createDate) {
//		super();
//		this.user = user;
//		this.userId = userId;
//		this.providerId = providerId;
//		this.providerUserId = providerUserId;
//		this.rank = rank;
//		this.displayName = displayName;
//		this.profileUrl = profileUrl;
//		this.imageUrl = imageUrl;
//		this.accessToken = accessToken;
//		this.secret = secret;
//		this.refreshToken = refreshToken;
//		this.expireTime = expireTime;
//		this.createDate = createDate;
//	}

	public SocialChannel (String userId, String providerId,
			String providerUserId, int rank, String displayName,
			String profileUrl, String imageUrl, String accessToken,
			String secret, String refreshToken, Long expireTime, Date createDate) {
		super();
		//this.user = user;
		this.userId = userId;
		this.providerId = providerId;
		this.providerUserId = providerUserId;
		this.rank = rank;
		this.displayName = displayName;
		this.profileUrl = profileUrl;
		this.imageUrl = imageUrl;
		this.accessToken = accessToken;
		this.secret = secret;
		this.refreshToken = refreshToken;
		this.expireTime = expireTime;
		this.createDate = createDate;
	}
	

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	public Key<User> getUser() {
//		return user;
//	}
//
//	public void setUser(Key<User> user) {
//		this.user = user;
//	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getProviderUserId() {
		return providerUserId;
	}

	public void setProviderUserId(String providerUserId) {
		this.providerUserId = providerUserId;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public Long getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Long expireTime) {
		this.expireTime = expireTime;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "SocialAgent [providerId=" + providerId + ", providerUserId="
				+ providerUserId + ", displayName=" + displayName
				+ ", accessToken=" + accessToken + ", secret=" + secret + "]";
	}

}
