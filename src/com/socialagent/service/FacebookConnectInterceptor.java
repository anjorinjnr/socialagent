package com.socialagent.service;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.web.ConnectInterceptor;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.request.WebRequest;

import com.socialagent.utils.AppUtil;

public class FacebookConnectInterceptor implements ConnectInterceptor<Facebook> {

	@Override
	public void postConnect(Connection<Facebook> con, WebRequest req) {

	}

	@Override
	public void preConnect(ConnectionFactory<Facebook> provider,
			MultiValueMap<String, String> parameters, WebRequest request) {
		AppUtil.print("fb intercepted");
		parameters.add("display", "popup");
	

	}

}
