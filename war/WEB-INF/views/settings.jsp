<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>


<div class="container">
	<h1>
		<span class="category">Settings</span>
		<hr />
	</h1>
	<div class="row">
		<ul id="homeTabs" class="nav nav-tabs">
			<li class="active"><a href="#agents" data-toggle="tab">Channels</a></li>
		</ul>


		<div id="tabContent" class="tab-content">
			<div class="tab-pane active" id="agents">
				<div class="btn-group">
					<a class="btn btn-sm btn-grove-one dropdown-toggle"
						data-toggle="dropdown" href="#">Add New Channel <span
						class="caret"></span>
					</a>
					<ul class="dropdown-menu grove-dropdown">
						<li><a id="facebook" href="#">Facebook</a></li>
						<li><a href="#">Twitter</a></li>
						<li><a href="#">SMS</a></li>
						<li><a href="#">Email</a></li>
					</ul>
				</div>
			</div>
		</div>

		<form id="fb_signin" action="<c:url value="/connect/facebook" />"
			method="post">
			<input type="hidden" name="scope"
				value="publish_stream,offline_access, manage_pages,read_page_mailboxes">
			<input type="hidden" name="displaytype" value="popup">
		</form>
	</div>

	<div class="channels row">

		<!-- <div class="row">
			<div class="col-md-6">
				<div>
					<p>
						<strong>Channel: </strong>Facebook
					</p>
					<img class="pull-right" style="height: 30px; width: 30px;"
						src="<c:url value="/assets/img/logos/fb.png" />" alt="Facebook">
				</div>

				<div class="channel-actions">
					<a href="#"> <i class="glyphicon glyphicon-cog"></i> Manage
						Pages
					</a> <a href="#"> <i class="glyphicon glyphicon-remove"></i> Delete
					</a>
				</div>
				<hr />
			</div>
		</div>  -->

		<c:forEach items="${socialChannels}" var="channel">
			<div class="row">
				<div class="col-md-6">
					<div>
						<p style="text-transform: capitalize">
							<strong>Channel: </strong>${channel.providerId}
						</p>
						<img class="pull-right" style="height: 30px; width: 30px;"
							src="<c:url value="/assets/img/logos/${channel.providerId}.png" />"
							alt="Facebook">
					</div>

					<div class="channel-actions">
						<a href="#" onclick="manageFb(${channel.id})"> <i
							class="glyphicon glyphicon-cog"></i> Manage Pages
						</a> <a href="#"> <i class="glyphicon glyphicon-remove"></i>
							Delete
						</a>
					</div>
					<hr />
				</div>
			</div>
		</c:forEach>
	</div>


</div>

<!-- Facebook Pages Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Manage Facebook Pages</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="save()">Save
					changes</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
	$(function() {

		$("#facebook").click(function() {

			event.preventDefault();
			window.open("", "connectWindow", "width=600,height=400");
			var form = $("#fb_signin");
			$(form).attr("target", "connectWindow");
			$(form).submit();
		});
	
	});
	
	function manageFb(channelId){

	   var url  = "<c:url value="/ajax/managepages/" />" + channelId;
	  $.get(url, function(data){
		  $(".modal-body").html(data);
		  $('#myModal').modal("show");
	  });
	}
	
	function save(){
		var pages = [];
		$("input[name=pagecheck]:checked").each(function(){
			var page = {};
			page.pageId = $(this).val();
			page.pageName = $(this).parent().find(".pagename").val();
			page.accessToken = $(this).parent().find(".access_token").val();
			pages.push(page);

		});
		
		if(pages.length > 0){
			
			$.ajax({
				  type: "POST",
				  url: "<c:url value="/ajax/managepages/" />" + channelId + "/save",
				  data: JSON.stringify(pages),
				  success: success,
				  dataType: dataType
				});
		}
		//console.log(pages);
		
	}
</script>
