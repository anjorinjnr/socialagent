package com.socialagent.web.config;

/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.connect.web.ConnectController;
//import org.springframework.social.connect.web.ConnectController;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;



//import com.socialagent.controller.ConnectController;
import com.socialagent.data.ObjectifyConnectionRepository;
import com.socialagent.data.ObjectifyUsersConnectionRepository;
import com.socialagent.data.SocialChannelDao;
import com.socialagent.domain.User;
import com.socialagent.service.FacebookConnectInterceptor;
import com.socialagent.utils.AppUtil;

@Configuration
@PropertySource("classpath:com/socialagent/web/config/application.properties")
public class SocialConfig {

	private static final Logger log = Logger.getLogger(SocialConfig.class
			.getName());

	@Autowired
	private SocialChannelDao socialAgentRepository;

	@Inject
	private Environment env;

	@Bean
	@Scope(value = "singleton", proxyMode = ScopedProxyMode.INTERFACES)
	public ConnectionFactoryLocator connectionFactoryLocator() {
		ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();
		registry.addConnectionFactory(new FacebookConnectionFactory(env
				.getProperty("facebook.clientId"), env
				.getProperty("facebook.clientSecret")));
		return registry;
	}

	/**
	 * Request-scoped data access object providing access to the current user's
	 * connections.
	 */
	@Bean
	@Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
	public ConnectionRepository connectionRepository() {
		User user = AppUtil.currentUser();// getLoginUserAccount();
		if (user == null) {
			throw new IllegalStateException(
					"Unable to get a ConnectionRepository: no user signed in");
		}
		log.info("returning  ObjectifyConnectionRepository");
		return usersConnectionRepository().createConnectionRepository(
				user.getEmail());
	}

	@Bean
	@Scope(value = "singleton", proxyMode = ScopedProxyMode.INTERFACES)
	public UsersConnectionRepository usersConnectionRepository() {
		if (socialAgentRepository != null){
			log.info("injected socialagent dao");
		}
		ObjectifyUsersConnectionRepository repository = new ObjectifyUsersConnectionRepository(
				socialAgentRepository, connectionFactoryLocator(),
				textEncryptor());
		// repository.setConnectionSignUp(autoConnectionSignUp());
		log.info("returning ObjectifyUsersConnectionRepository");
		return repository;

	}

	//
	public TextEncryptor textEncryptor() {

		return Encryptors.queryableText(
				env.getProperty("security.encryptPassword"),
				env.getProperty("security.encryptSalt"));
	}

	@Bean
	public ConnectController connectController() {
		// AppUtil.print("in connect controller");
		ConnectController con = new ConnectController(
				connectionFactoryLocator(), connectionRepository());
		con.setApplicationUrl(env.getProperty("application.url"));
		con.addInterceptor(new FacebookConnectInterceptor());
		return con;
	}

	@Bean
	@Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
	public Facebook facebook() {
		log.info("after access token, hand over to fb");
		Connection<Facebook> facebook = connectionRepository()
				.findPrimaryConnection(Facebook.class);

		return facebook != null ? facebook.getApi() : new FacebookTemplate();
	}

}
