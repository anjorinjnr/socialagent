package com.socialagent.data;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;
import org.springframework.util.MultiValueMap;

import com.google.common.collect.Lists;
import com.googlecode.objectify.Key;
import com.socialagent.domain.SocialChannel;
import com.socialagent.domain.User;
import com.socialagent.utils.AppUtil;

@Repository
public class SocialChannelDaoObjectify extends GenericDaoObjectify<SocialChannel>
		implements SocialChannelDao {

	public SocialChannelDaoObjectify() {
		super(SocialChannel.class);
	}

	@Override
	public List<SocialChannel> findByUserId(String userId) {
		List<SocialChannel> agents = ofy().query(SocialChannel.class)
				.filter("userId", userId).list();
		return agents;
	}

	@Override
	public List<SocialChannel> findByUserIdAndProviderId(String userId,
			String providerId) {
		
		List<SocialChannel> agents = ofy().query(SocialChannel.class)
				.filter("userId", userId).filter("providerId", providerId)
				.list();
		
		if(AppUtil.listIsNullOrEmpty(agents))
			AppUtil.print("agents not found");
		return agents;
	}

	@Override
	public List<SocialChannel> findByProviderIdAndProviderUserId(
			String providerId, String providerUserId) {
		List<SocialChannel> agents = ofy().query(SocialChannel.class)
				// .filter("user", new Key<User>(User.class, userId))
				.filter("providerId", providerId)
				.filter("providerUserId", providerUserId).list();
		return agents;
	}

	@Override
	public SocialChannel findByUserIdAndProviderIdAndProviderUserId(String userId,
			String providerId, String providerUserId) {
		SocialChannel agent = ofy().query(SocialChannel.class)
				.filter("userId", userId)
				.filter("providerId", providerId)
				.filter("providerUserId", providerUserId).get();
		return agent;
	}

	@Override
	public List<SocialChannel> findByProviderIdAndProviderUserIds(
			String providerId, Collection<String> providerUserIds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUser(String userId) {
		return ofy().query(User.class).filter("email", userId).get();
	}
}
