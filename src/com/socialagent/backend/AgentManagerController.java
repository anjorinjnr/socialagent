package com.socialagent.backend;

import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.socialagent.service.AgentManagementService;

@SuppressWarnings("serial")
@Controller
public class AgentManagerController extends HttpServlet {

	private AgentManagementService service;

	@Autowired
	public void setService(AgentManagementService service) {
		this.service = service;
	}

	private static final Logger log = Logger
			.getLogger(AgentManagerController.class.getName());

	private void start() {
		if (service != null) {
			log.info("Starting Agent Manager in backend");
			service.start();
		} else {
			log.severe("AgentManagementService is missing");
		}
	}

	@RequestMapping(value = "/_ah/start", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public void background() {
		start();
	}

}
