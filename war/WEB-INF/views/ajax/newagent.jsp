<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
.row {
	padding-bottom: 10px;
}

.form-control {
	font-size: 14px;
}
</style>
<div>
	<div class="row">
		<div class="form-group">
			<label for="name" class="col-sm-3">Agent Name</label>
			<div class="col-sm-6">
				<input id="name" class="form-control input-lg" type="text"
					placeholder="Name/Title">
			</div>
		</div>
	</div>

	<div class="row">
		<div class="form-group">
			<label for="description" class="col-sm-3">Description</label>
			<div class="col-sm-6">
				<textarea class="form-control" id="description" rows="3"
					placeholder="Description"></textarea>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<label for="description" class="col-sm-3">Channel</label>
			<div class="col-sm-6">
				<select id="channel" class="form-control">
					<option>Select Channel</option>
					<c:forEach items="${channels}" var="channel">
						<option value="${channel.id}">${channel.providerId}</option>
					</c:forEach>

				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<div id="channelOption" class="col-sm-6 col-sm-offset-3"></div>
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<label for="description" class="col-sm-3">Message</label>
			<div class="col-sm-6">
				<textarea class="form-control" id="message" rows="3"></textarea>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<label for="description" class="col-sm-3">EndPoint</label>
			<div class="col-sm-6">
				<select id="endpoint" class="form-control">
					<option>API</option>
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<div class="col-sm-6">
				<input type="text" id="url" name="url" class="input-text-light"
					placeholder="Enter endpoint URL here">
			</div>
			<div class="col-sm-3">
				<select id="api-verb" class="form-control">
					<option value="get">GET</option>
					<option value="post">POST</option>
				</select>
			</div>
			<div class="col-sm-">
				<button type="button btn-default" class="btn" id="params">
					<i class="glyphicons edit"><i></i></i><span>URL Params</span>
				</button>
			</div>
		</div>
	</div>
	<div id="url-keyvaleditor">
		<div class="keyvalueeditor-row">
			<input id="def" type="text"
				class="keyvalueeditor-key input-text-light"
				placeholder="URL Parameter Key" name="keyvalueeditor-key"> <input
				type="text" class="keyvalueeditor-value keyvalueeditor-value-text"
				placeholder="Value" name="keyvalueeditor-value">
		</div>
	</div>
</div>
<div id="param-keys">
	<div class="keyvalueeditor-row">
		<input type="text" class="keyvalueeditor-key input-text-light"
			placeholder="URL Parameter Key" name="keyvalueeditor-key"> <input
			type="text" class="keyvalueeditor-value keyvalueeditor-value-text"
			placeholder="Value" name="keyvalueeditor-value"> <a
			tabindex="-1" class="keyvalueeditor-delete"
			onclick="removeRow(this);"><img class="deleteButton"
			src="<c:url value="/assets/img/delete.png"/>"></a>
	</div>
</div>
<script>
	function notnull(e) {
		if (e != "" && e != null && typeof e != 'undefined')
			return true;
		else
			return false;
	}
	function createAgent() {
		var agent = {};
		agent.name = $("#name").val();
		agent.description = $("#description").val();
		agent.channel = $("#channel option:selected").text();
		agent.channelId = $("#channel").val();
		agent.channelOptions = [];
		if(agent.channel == "facebook"){
			$("input[name=pagecheck]:checked").each(function(){
				var page = {};
				page.id = $(this).val();
				page.name = $(this).parent().find(".pagename").val();
				page.access_token = $(this).parent().find(".access_token").val();
				agent.channelOptions.push(page);

			});
		}
		agent.message = $("#message").val();
		agent.api = $("#endpoint option:selected").text();
		agent.url = $("#url").val();
		agent.requestType = $("#api-verb option:selected").text();
		agent.params = [];
		$("#url-keyvaleditor .keyvalueeditor-row").each(function() {
			var param = {};
			param.key = $(this).find(".keyvalueeditor-key").val();
			param.value = $(this).find(".keyvalueeditor-value").val();
			if (notnull(param.key) && notnull(param.value))
				agent.params.push(param);
		});
		//console.log(agent);
		$.ajax({
			  type: "POST",
			  url: "<c:url value="/ajax/newagent" />",
			  data: {"data": JSON.stringify(agent)},
			  success: function(data){
				  console.log(data);
			  },
			  dataType: "text"
			});
	}
	function removeRow(e) {
		$(e).parents(".keyvalueeditor-row").remove();
	}
	$(function() {
		//"/ajax/channel/options/{channel}/{channelId}
		$("#channel")
				.change(
						function() {

							//return;
							$("#channelOption").html("");
							if ($("#channel option:selected").text() != "Select Channel") {
								var url = "<c:url value="/ajax/channel/options/" />"
										+ $("#channel option:selected").text()
										+ "/" + $("#channel").val();
								$.get(url, function(data) {
									$("#channelOption").html(data);

								});
							}

						});

		$(document).on("focus", ".keyvalueeditor-key", function() {
			console.log(";focus")
			if ($(this).parent().next(".keyvalueeditor-row").length < 1) {
				$("#url-keyvaleditor").append($("#param-keys").html());
			}
		});

		$("#param-keys").hide();

		$("#api-verb").change(
				function() {
					if ($(this).val() == "get") {
						$("#params span").text("URL Params");
						$(".keyvalueeditor-key").attr("placeholder",
								"URL Parameter Key")

					} else {
						$("#params span").text("Form Data");
						$(".keyvalueeditor-key").attr("placeholder", "Key")
					}
				})

		$("#params").click(function() {
			if ($("#url-keyvaleditor").is(":hidden")) {
				$("#url-keyvaleditor").show();
				$(this).addClass("active");
			} else {
				$(this).removeClass("active");
				$("#url-keyvaleditor").hide();
			}
			/* console.log("params")
			$(".url-keyvaleditor").append($("#param-keys").html()); */
		})
	})
</script>