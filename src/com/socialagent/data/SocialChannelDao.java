package com.socialagent.data;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.util.MultiValueMap;

import com.socialagent.domain.SocialChannel;
import com.socialagent.domain.User;

public interface SocialChannelDao extends GenericDao<SocialChannel> {

	List<SocialChannel> findByUserId(String userId);

	List<SocialChannel> findByUserIdAndProviderId(String userId, String providerId);

	List<SocialChannel> findByProviderIdAndProviderUserId(String providerId,
			String providerUserId);

	SocialChannel findByUserIdAndProviderIdAndProviderUserId(String userId,
			String providerId, String providerUserId);

	List<SocialChannel> findByProviderIdAndProviderUserIds(String providerId,
			Collection<String> providerUserIds);
	
	User getUser(String userId);

	/*
	 * 
	 * List<SocialAgent> findByUserIdAndProviderUserIds(String userId,
	 * MultiValueMap<String, String> providerUserIds);
	 * 
	 * SocialAgent get(String userId, String providerId, String providerUserId);
	 * 
	 * List<SocialAgent> findPrimaryByUserIdAndProviderId(String userId, String
	 * providerId);
	 * 
	 * Integer selectMaxRankByUserIdAndProviderId(String userId, String
	 * providerId);
	 * 
	 * List<String> findUserIdsByProviderIdAndProviderUserId(String providerId,
	 * String providerUserId);
	 * 
	 * List<String> findUserIdsByProviderIdAndProviderUserIds(String providerId,
	 * Set<String> providerUserIds);
	 */

}