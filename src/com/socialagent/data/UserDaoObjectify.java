package com.socialagent.data;

import org.springframework.stereotype.Repository;

import com.socialagent.domain.User;
import com.socialagent.exception.InvalidLoginException;

@Repository
public class UserDaoObjectify extends GenericDaoObjectify<User> implements UserDao {

	
	
	public UserDaoObjectify() {
		super(User.class);
		
	}

	@Override
	public User login(String username, String password)
			throws InvalidLoginException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUserByEmail(String email) {
		User user = ofy().query(User.class).filter("email", email).get();
		return user;
	}


}
