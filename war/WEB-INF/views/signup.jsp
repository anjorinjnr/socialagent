
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="container">


	<h1>
		<span class="category">Sign Up</span>
		<hr />
	</h1>

	<form action="<c:url value="/signup" /> " method="post">
		<div class="row">
			<div class="col-sm-4 form-group">
				<label for="email">Company Name</label> <input
					class="form-control input-lg" type="text" id="companyName"
					name="companyName" value="" placeholder="">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 form-group">
				<label for="email">Full Name</label> <input
					class="form-control input-lg" type="text" id="fullName"
					name="fullName" value="" placeholder="">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 form-group">
				<label for="email">Email</label> <input
					class="form-control input-lg" type="email" id="email" name="email"
					value="" placeholder="">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 form-group">
				<label for="email">Password</label> <input
					class="form-control input-lg" type="password" id="password" name="password"
					value="" placeholder="">
			</div>
			<div class="col-sm-4 form-group">
				<label for="email">Confirm Password</label> <input
					class="form-control input-lg" type="password" id="confPassword" name="confPassword"
					value="" placeholder="">
			</div>
		</div>
		<button class="btn btn-grove-one btn-lg btn-bold" type="submit">Sign
			up</button>
		<div class="clearfix"></div>
	</form>
</div>
