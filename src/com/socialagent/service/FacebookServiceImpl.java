package com.socialagent.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.mortbay.log.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import com.socialagent.domain.FacebookAgent;
import com.socialagent.domain.PageList;
import com.socialagent.utils.AppUtil;

@Service
public class FacebookServiceImpl {

	// https://graph.facebook.com/me/accounts?method=GET&format=json&suppress_http_code=1&access_token=CAACEdEose0cBAPJW0hpkbXERx0AZAhmoehZAbnXU52bQM8gOv4n4lKV1zUNLC84CKPEviiacChwQbZBcnv8nCtZBnUZAdTWz6LaAjMmHyuzHQmuB7U1FM2NHDDksggI0a3ZBcisS0kmUMvSAwzZC8UZAGZAZA5OVZBnZBBUCzYAsfPUygUtEvcFWhFZANSEBIV3HFptoJX1R1ZA9RTrwZDZD

	private final static String GRAPH_URL = "https://graph.facebook.com";
	private final static String PAGES_URL = "/me/accounts";


	private UsersConnectionRepository usersConnectionRepository;

	private static final Logger log = Logger
			.getLogger(FacebookServiceImpl.class.getName());

	@Autowired
	public FacebookServiceImpl(
			UsersConnectionRepository usersConnectionRepository) {

		this.usersConnectionRepository = usersConnectionRepository;

	}



	public PageList getPages() {
		if (usersConnectionRepository == null) {
			Log.info("did not inject usersConnectionRepository");
			return null;
		}
		PageList pageList = null;
		ConnectionRepository cr = usersConnectionRepository
				.createConnectionRepository(AppUtil.currentUser().getEmail());

		RestOperations fb = cr.getPrimaryConnection(Facebook.class).getApi()
				.restOperations();
		Map<String, String> param = new HashMap<>();
		param.put("method", "GET");
		param.put("method", "json");
		param.put("suppress_http_code", "1");
		ResponseEntity<String> pages = fb.getForEntity(GRAPH_URL + PAGES_URL,
				String.class, param);
		ObjectMapper mapper = new ObjectMapper();
		try {
			// PageList pagelist = mapper.readValue(
			// pages.getBody(),
			// TypeFactory.defaultInstance().constructCollectionType(
			// List.class, PageList.class));
			pageList = mapper.readValue(pages.getBody(), PageList.class);

		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			log.info(e.toString());
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			log.info(e.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.info(e.toString());
		}
		return pageList;
	}
}
