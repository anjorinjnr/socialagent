<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>

<meta charset="utf-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">

<!-- Glyphicons -->
<link
	href="<c:url value="/assets/css/vendor/glyphicons/glyphicons.css"/>"
	rel="stylesheet">

<!-- Google Webfonts -->
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600'
	rel='stylesheet' type='text/css'>

<!--  insert dynamic style list -->
<tiles:useAttribute id="styles" name="styles" classname="java.util.List" />
<c:forEach var="css" items="${styles}">
	<link href="<c:url value="${css}" />" rel="stylesheet" type="text/css" />
</c:forEach>

<!--  insert dynamic style list -->
<tiles:useAttribute id="otherstyles" name="otherstyles"
	classname="java.util.List" />
<c:forEach var="css" items="${otherstyles}">
	<link href="<c:url value="${css}" />" rel="stylesheet" type="text/css" />
</c:forEach>

<!--[if lt IE 9]>
        <link rel="stylesheet" href="css/ie8.css">        
        <script src="js/vendor/google/html5-3.6-respond-1.1.0.min.js"></script>
    <![endif]-->

<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>

</head>
<body>

	<div id="wrap">
		<header>
			<tiles:insertAttribute name="header" />
		</header>

		<div class="widewrapper maincontent">
			<tiles:insertAttribute name="body" />
		</div>
	</div>

	<footer class="widewrapper footer">
		<div class="container">
			<div class="row footer">
				<div class="col-sm-12 text-center">
					<ul class="list-inline">
						<li><a href="#">About</a></li>
						<li><a href="#">Blog</a></li>
						<li><a href="#">Terms</a></li>
						<li><a href="#">Privacy</a></li>
					</ul>
					<a href="#" class="glyphicons facebook"><i></i></a> <a href="#"
						class="glyphicons twitter"><i></i></a> <a href="#"
						class="glyphicons google_plus"><i></i></a> <a href="#"
						class="glyphicons linked_in"><i></i></a>

					<p>
						<br />Social Agent &copy 2013. Built in PGH.
					</p>
				</div>


			</div>
		</div>


	</footer>

	<!--  insert javascripts -->
	<tiles:useAttribute id="scripts" name="scripts"
		classname="java.util.List" />
	<c:forEach var="js" items="${scripts}">
		<script type="text/javascript" src="<c:url value="${js}" />"></script>
	</c:forEach>

	<!--  insert javascripts -->
	<tiles:useAttribute id="otherjs" name="otherjs"
		classname="java.util.List" />
	<c:forEach var="js" items="${otherjs}">
		<script type="text/javascript" src="<c:url value="${js}" />"></script>
	</c:forEach>

</body>
</html>