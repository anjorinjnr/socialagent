package com.socialagent.service;

import java.util.List;

import com.socialagent.domain.SocialChannel;

public interface ChannelService {

	List<SocialChannel> getSocialChannels();
}
