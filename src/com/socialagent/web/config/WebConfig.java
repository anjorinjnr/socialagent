
package com.socialagent.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles2.TilesConfigurer;
import org.springframework.web.servlet.view.tiles2.TilesView;

import com.googlecode.objectify.spring.ObjectifyFactoryBean;
import com.socialagent.backend.AgentManagerController;
import com.socialagent.utils.AppUtil;

/**
 * Spring MVC Configuration
 * 
 * 
 * @author Ebenezer Anjorin
 */
@EnableWebMvc
@Configuration
@ComponentScan(basePackages = {
	    "com.socialagent.controller",
	    "com.socialagent.service",
	    "com.socialagent.data",
	    "com.socialagent.web.config",
	    "com.socialagent.backend"
	})

public class WebConfig extends WebMvcConfigurerAdapter {

	
	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
	
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/assets/**").addResourceLocations("WEB-INF/assets/").setCachePeriod(0);
    }
    
    @Bean
    public ViewResolver viewResolver(){
    	UrlBasedViewResolver vr =new UrlBasedViewResolver();
        vr.setOrder(1);
        vr.setViewClass(TilesView.class);
        return vr;
    }
    
    @Bean
    public TilesConfigurer tilesConfigurer(){
    	TilesConfigurer tc = new TilesConfigurer();
    	tc.setDefinitions( new String[]{"/WEB-INF/tiles.xml"});
        return tc;
    }
    
    @Bean
    public ObjectifyFactoryBean objectifyFactoryBean(){
    	ObjectifyFactoryBean ofb = new ObjectifyFactoryBean();
    	ofb.setBasePackage("com.socialagent.domain");
    	return ofb;
    }
    
//    @Bean
//    public AgentManagerServlet agentManager(){
//    	AgentManagerServlet am = new AgentManagerServlet();
//    	return am;
//    }
}
