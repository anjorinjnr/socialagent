package com.socialagent.data;

import com.socialagent.domain.FacebookAgent;

import com.socialagent.exception.InvalidLoginException;

public interface  FacebookAgentDao extends GenericDao<FacebookAgent>{

}
