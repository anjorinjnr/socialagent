package com.socialagent.data;

import com.socialagent.domain.User;
import com.socialagent.exception.InvalidLoginException;

public interface  UserDao extends GenericDao<User>{

	User login(String username, String password) throws InvalidLoginException;
	User getUserByEmail(String email);

}
