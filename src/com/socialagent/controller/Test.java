package com.socialagent.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.socialagent.utils.AppUtil;

public class Test {

	public static void main(String[] args) throws JsonProcessingException,
			FileNotFoundException, IOException {
		ObjectMapper m = new ObjectMapper();
		// can either use mapper.readTree(source), or mapper.readValue(source,
		// JsonNode.class);
		JsonNode rootNode = m
				.readTree(new FileInputStream(
						new File(
								"C:\\Users\\Tola\\Documents\\workspace-sts-3.4.0.RELEASE\\socialagent\\src\\com\\socialagent\\controller\\msg.json")));
		// AppUtil.print(rootNode.toString());
		// ensure that "last name" isn't "Xmler"; if is, change to "Jsoner"
		JsonNode root = rootNode.path("data");

		Iterator<JsonNode> data = root.getElements();
		while (data.hasNext()) {
			JsonNode current = data.next();
			JsonNode messages = current.path("messages");
			JsonNode msgData = messages.path("data");
			Iterator<JsonNode> message = msgData.getElements();
			while (message.hasNext()) {
				JsonNode msg = message.next();
				if(msg.path("from").get("id").equals("111850665561577")){
					continue;
				}
				
				AppUtil.print(msg.get("message").getValueAsText());
			}
		}

		//AppUtil.print(dataNode.toString());
		// String lastName = nameNode.path("last").getTextValue().
		// if ("xmler".equalsIgnoreCase(lastName)) {
		// ((ObjectNode)nameNode).put("last", "Jsoner");
		// }
		// // and write it out:
		// m.writeValue(new File("user-modified.json"), rootNode);

	}
}
