package com.socialagent.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialagent.data.FacebookAgentDao;
import com.socialagent.domain.Agent;
import com.socialagent.domain.FacebookAgent;

@Service
public class AgentServiceImpl  implements AgentService{

	private FacebookAgentDao agentDao;
	
	@Autowired
	public AgentServiceImpl(FacebookAgentDao agentDao) {
		this.agentDao = agentDao;
	}


	@Override
	public void addAgent(FacebookAgent agent) {
		agentDao.save(agent);
		
	}

	
	/*
	 * this method gets all agents in the repository when started, there needs to be a 
	 * way to manage agents lists in real time, so when new agents are added or deleted
	 * the list is updated; I will worry about this later.
	 * @see com.socialagent.service.AgentService#getAgents()
	 */
	@Override
	public List<FacebookAgent> getAgents() {
		return agentDao.getAll();
	}
	

}
