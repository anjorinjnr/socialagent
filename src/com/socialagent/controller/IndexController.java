package com.socialagent.controller;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.socialagent.data.SocialChannelDao;
import com.socialagent.domain.SocialChannel;
import com.socialagent.domain.User;
import com.socialagent.exception.InvalidLoginException;
import com.socialagent.service.ChannelService;
import com.socialagent.service.UserService;
import com.socialagent.utils.AppUtil;

@Controller
public class IndexController {

	UserService userService;
	SocialChannelDao scDao;// channelService; , SocialChannelDao scDao

	@Autowired
	public IndexController(UserService userService, SocialChannelDao scDao) {
		this.userService = userService;
		this.scDao = scDao;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signup(User user, HttpSession session) {
		userService.signup(user);
		session.setAttribute(AppUtil.CURRENT_USER, user);
		
//		SocialChannel sc = new SocialChannel();
//		sc.setAccessToken("a036faa382f69fee5364abdcb5da0bb8d337eb6181520bc81fd14f3ca6111cd18bc2493738c59bc9eaa1a20545535529e9f3b38ac4f9abf01f48004745b47d97c044ca94f59e1b75cc451049c8db10a740aeb715896df69e6ad31e0f59beb634a195743e8ab6d5d001e085e6d5570796ec51c4910c3db6e184768e48f982448c7ef6ea76e0f31d558add4756ee6eb98c067aa2aa237151d8cb6ab61b1ed71db74ac9d09615091a830aa84a1a8e2d42d44f6c88d1972655e31914860efa40d8cb8b4d76562e183bf8d416c1b0bff0f44d");
//		sc.setCreateDate(new Date());
//		sc.setDisplayName("anjorinjnr");
//		sc.setExpireTime(new Long("1388731984304"));
//		sc.setImageUrl("http://graph.facebook.com/587182748/picture");
//		sc.setProfileUrl("http://facebook.com/profile.php?id=587182748");
//		sc.setProviderId("facebook");
//		sc.setUserId("tola.anjorin@gmail.com");
//		this.scDao.save(sc);
		return "redirect:/home";

	}

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup() {
		return "signup";

	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@RequestParam String email, @RequestParam String password, HttpSession session)  {
		try {
			User user = userService.login(email, password);
			session.setAttribute(AppUtil.CURRENT_USER, user);
			
		} catch (InvalidLoginException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "redirect:/home";
	}

}
