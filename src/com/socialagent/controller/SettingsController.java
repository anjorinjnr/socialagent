package com.socialagent.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.socialagent.domain.FacebookPage;
import com.socialagent.domain.PageList;
import com.socialagent.domain.SocialChannel;
import com.socialagent.service.ChannelService;
import com.socialagent.service.FacebookServiceImpl;
import com.socialagent.service.UserService;
import com.socialagent.utils.AppUtil;

@Controller
public class SettingsController {

	
	ChannelService channelService;
	FacebookServiceImpl fbService;
	
	@Autowired
	public SettingsController(ChannelService channelService, FacebookServiceImpl fbService){
		
		this.channelService = channelService;
		this.fbService = fbService;
	}
	
	@RequestMapping(value = "/settings", method = RequestMethod.GET)
	public String settings(Model model){
	
		model.addAttribute("user", AppUtil.currentUser());
		List<SocialChannel> scs = channelService.getSocialChannels();
//		if(scs == null || scs.size() == 0)
//			AppUtil.print("no channel found");
//		else{
			model.addAttribute("socialChannels", channelService.getSocialChannels());
//			this.fbService.getPages();
//		}
		
		return "settings";
	}
	
	@RequestMapping(value = "/ajax/managepages/{channelId}", method = RequestMethod.GET)
	public String managePageView(@PathVariable String channelId, Model model){
		
//		List<FacebookPage> pgs = new ArrayList<>();
//		FacebookPage fb1 = new FacebookPage();
//		fb1.setAccess_token("CAACEdEose0cBAMuOLtCG3n3i65H0sc3OBOf9P2KIZBrHWBQHww204L8HFXyF4lReWPMg4yXIFCDZCUe1nBhw4ttvX0N9f9blmWpJJRwBVYJjbZCHVGsJ7jJq0n44LrOWB4voTriTfVJaxJgndm07j9BtqPGE4a8EZBDBdL4cRGTGE3PMSVc72IvCxcSXTS4ZD");
//		fb1.setCategory("Internet/software");
//		fb1.setId("111850665561577");
//		fb1.setName("Adloopz");
//		pgs.add(fb1);
//		FacebookPage fb2 = new FacebookPage();
//		fb2.setAccess_token("CAACEdEose0cBAMuOLtCG3n3i65H0sc3OBOf9P2KIZBrHWBQHww204L8HFXyF4lReWPMg4yXIFCDZCUe1nBhw4ttvX0N9f9blmWpJJRwBVYJjbZCHVGsJ7jJq0n44LrOWB4voTriTfVJaxJgndm07j9BtqPGE4a8EZBDBdL4cRGTGE3PMSVc72IvCxcSXTS4ZD");
//		fb2.setCategory("Consulting/business services");
//		fb2.setId("274472322596421");
//		fb2.setName("HB Careers");
//		pgs.add(fb2);
		
		
		PageList fbPageList = fbService.getPages();
		model.addAttribute("pageList", fbPageList.getData());
		//model.addAttribute("pageList",pgs);
		return "fbpages";
	}
	@RequestMapping(value = "/ajax/managepages/{channelId}/save", method = RequestMethod.GET)
	public void saveFacebookPages(@RequestParam String pagesJson){
		AppUtil.print(pagesJson);
	}
}
