package com.socialagent.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialagent.data.SocialChannelDao;
import com.socialagent.domain.SocialChannel;
import com.socialagent.utils.AppUtil;

@Service
public class ChannelServiceImpl implements ChannelService {

	private SocialChannelDao socialChannelDao;

	@Autowired
	public ChannelServiceImpl(SocialChannelDao socialChannelDao) {
		this.socialChannelDao = socialChannelDao;
	}

	public List<SocialChannel> getSocialChannels() {
		return this.socialChannelDao.findByUserId(AppUtil.currentUser().getEmail());
		// socialChannelDao.getAll();// 

}

}
