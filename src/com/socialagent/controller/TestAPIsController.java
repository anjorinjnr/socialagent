package com.socialagent.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.socialagent.domain.SocialAgentResponse;

@Controller
public class TestAPIsController {

	@RequestMapping(value = "/testapis/usps/tracking", method = RequestMethod.GET)
	public @ResponseBody SocialAgentResponse upsTracking(@RequestParam String trackingNumber) {

		Map<String, String> dummyDb = new HashMap<>();
		dummyDb.put(
				"9400110200883011342900",
				"Your item was delivered at 1:34 pm on December 13, 2013 in PITTSBURGH, PA 15213");

		if (dummyDb.containsKey(trackingNumber))
			return new SocialAgentResponse("success",
					dummyDb.get(trackingNumber));
		else
			return new SocialAgentResponse("error",
					"Tracking Number does not exist");

	}
}
