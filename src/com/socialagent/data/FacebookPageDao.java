package com.socialagent.data;

import com.socialagent.domain.FacebookPage;
import com.socialagent.domain.User;
import com.socialagent.exception.InvalidLoginException;

public interface  FacebookPageDao extends GenericDao<FacebookPage>{

}
