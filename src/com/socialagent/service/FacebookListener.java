package com.socialagent.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.web.client.RestOperations;

import com.socialagent.domain.FacebookPage;
import com.socialagent.utils.AppUtil;

public class FacebookListener implements Runnable {

	private final String GRAPH_URL = "https://graph.facebook.com/";
	private final String HOST = "graph.facebook.com";
	private String path;

	private static final Logger log = Logger.getLogger(FacebookListener.class
			.getName());

	private RestOperations fb;

	private FacebookPage page;
	// private Map<String, String> param = new HashMap<>();

	private static Set<String> SEEN = new HashSet<>();

	private final HttpClient httpclient = new DefaultHttpClient();
	private List<NameValuePair> params = new ArrayList<>();
	private URIBuilder ub;

	public FacebookListener(RestOperations fb, FacebookPage page)
			throws URISyntaxException {

		this.fb = fb;
		this.page = page;
		if (page != null) {
			path = page.getId() + "/conversations";
			ub = new URIBuilder(GRAPH_URL + path);
			ub.addParameter("method", "GET");
			ub.addParameter("format", "json");
			ub.addParameter("suppress_http_code", "1");
			ub.addParameter("access_token", page.getAccess_token());
		}
	}

	@Override
	public void run() {
		while (true) {

			if (page == null || fb == null) {
				log.severe("Cannot run task without the FacebookPage and the FaceBook RestOperations objects");
				return;
			}
	
			try {
				HttpGet httpget = new HttpGet(ub.build());
				//log.info("sending request to :" + httpget.getURI());
				HttpResponse response = httpclient.execute(httpget);
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					String jsonResponse = EntityUtils.toString(entity);
					ObjectMapper mapper = new ObjectMapper();
					JsonNode rootNode = null;

					//log.info("API Response: " + jsonResponse);
					rootNode = mapper.readTree(jsonResponse);
					// get data node
					JsonNode root = rootNode.path("data");
					// get conversations
					Iterator<JsonNode> data = root.getElements();
					while (data.hasNext()) {
						JsonNode current = data.next();
						JsonNode messages = current.path("messages");
						JsonNode msgData = messages.path("data");
						Iterator<JsonNode> message = msgData.getElements();
						while (message.hasNext()) {
							JsonNode msg = message.next();
							// ignore sent messages
							if (msg.path("from").get("id").equals(page.getId())) {
								continue;
							}
							String msgId = msg.get("id").getValueAsText();
							//ignore if already seen
							if (SEEN.contains(msgId))
								continue;
							// process new message 
							AppUtil.print("Message: "
									+ msg.get("message").getValueAsText());
							// mark as seen
							SEEN.add(msgId);
						}
					}
				}

			} catch (JsonProcessingException e) {
				log.severe(e.getMessage());
			} catch (URISyntaxException e) {
				log.severe(e.getMessage());
			} catch (IOException e) {
				log.severe(e.getMessage());
			}

			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				log.severe(e.getMessage());
			}

		}
	}
}
