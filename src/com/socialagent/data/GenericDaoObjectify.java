package com.socialagent.data;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.socialagent.domain.DomainObject;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;

@Repository
public class GenericDaoObjectify<T extends DomainObject> implements
		GenericDao<T> {

	protected ObjectifyFactory objectifyFactory;
	private Class<T> type;

	@Autowired
	public void setObjectifyFactory(ObjectifyFactory objectifyFactory) {
		this.objectifyFactory = objectifyFactory;
	}

	protected Objectify ofy() {
		return objectifyFactory.begin();
	}

	public GenericDaoObjectify() {

	}

	public GenericDaoObjectify(Class<T> type) {
		super();
		this.type = type;
	}

	@Override
	public T get(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> getAll() {
		List<T> results = ofy().query(type).list();
		return results;
	}

	@Override
	public boolean save(T object) {
		Key<T> key = ofy().put(object);
		return (key != null) ? true : false;

	}

	@Override
	public void delete(T object) {
		// TODO Auto-generated method stub

	}

}
