<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

    <!--LayerSlider begin-->

    <!-- Note: Layerslider is configured in js/grove-slider.js -->
    <div class="hidden-xs">
<!--         <div id="layerslider" style="width: 100%; height: 405px; margin: 0px auto; ">
            <div class="ls-layer" style="transition2d:5;">
                <img class="ls-bg" src="img/slides/bg-five.jpg" alt="Slider Background">

                <img  class="ls-s-1" src="img/slides/imac.png" alt="iMac" 
                      style="top:15px; left:650px; durationin : 1250; durationout : 1250;">

                <div class="ls-s-1 large-caption" 
                     style="top:120px; left:20px; delayin : 750; slidedirection : fade; slideoutdirection : fade;">
                    <div><p>Introducing Grove:</p></div>
                    <div><p>A clean, responsive and flexible</p></div>
                    <div>
                        <p>business theme.</p>
                    </div>
                </div>
            </div>

            <div class="ls-layer" style="transition2d:5;">
                <img class="ls-bg" src="img/slides/bg-one.jpg" alt="Slider Background">

                <img  class="ls-s-1" src="img/slides/fam_imac.png" alt="iMac" 
                      style="top:50px; left:50px; durationin : 1250; durationout : 1250; delayin : 0;">
                <img  class="ls-s-1" src="img/slides/fam_macbook.png" alt="MacBook" 
                      style="top:190px; left:250px; durationin : 1250; durationout : 1250; delayin : 100;">
                <img  class="ls-s-1" src="img/slides/fam_ipad.png" alt="iPad" 
                      style="top:210px; left:0px; durationin : 1250; durationout : 1250; delayin : 200;">
                <img  class="ls-s-1" src="img/slides/fam_iphone.png" alt="iPhone" 
                      style="top:265px; left:160px; durationin : 1250; durationout : 1250; delayin : 300;">

                <div class="ls-s-1 large-caption" 
                     style="top:130px; left:700px; delayin : 700; slidedirection : fade; slideoutdirection : fade;">
                    <div><p>Layerslider:</p></div>
                    <div><p>Highly configurable,</p></div>
                    <div><p>fexible, responsive and fast</p></div>
                </div>

            </div>   

             <div class="ls-layer" style="transition2d:5;">
                <img class="ls-bg" src="img/slides/bg-three.jpg" alt="Slider Background">

                <img  class="ls-s-1" src="img/slides/macbook-air.png" alt="MB Air" 
                      style="top:25px; left:50px; durationin : 1250; durationout : 1250;">

                <div class="ls-s-1 large-caption" 
                     style="top:150px; left:750px; delayin : 750; slidedirection : fade; slideoutdirection : fade;">
                    <div><p>Impress your visitors,</p></div>
                    <div><p>and turn them into clients!</p></div>
                </div>
            </div>
        </div> -->
    </div>
    
    <!--LayerSlider end-->

    <!-- Layerslider substitute on x-small devices -->
    <div class="widewrapper pagetitle visible-xs">
        <div class="container">
            <h1>Welcome to Social Agent!</h1>        
        </div>
    </div> 

   <div class="widewrapper weak-highlight">
        <div class="container content our-clients">
            <div class="showroom-controls">
                <div class="links">Our clients</div>
            </div>
            <div class="clients">
                <div class="client"><img src="<c:url value="/assets/img/logos/logo1.png" />" class="img-responsive" alt="A Bright Idea"></div>
                <div class="client"><img src="<c:url value="/assets/img/logos/logo2.png" />" class="img-responsive" alt="U Read"></div>
                <div class="client"><img src="<c:url value="/assets/img/logos/logo3.png" />" class="img-responsive" alt="Drop"></div>
                <div class="client"><img src="<c:url value="/assets/img/logos/logo4.png" />" class="img-responsive" alt="Alaska Anchor"></div>
            </div>
        </div>
    </div>  