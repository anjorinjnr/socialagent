<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<nav class="navbar navbar-default grove-navbar" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<a href="#" class="grove-toggle" data-toggle="collapse"
				data-target=".grove-nav"> <i class="glyphicons show_lines"><i></i></i>
			</a> <a href="<c:url value="/" />" class="navbar-brand"><img
				src="<c:url value="/assets/img/logo.png" />" alt="Social Agent"></a>
		</div>

		<div class="collapse navbar-collapse grove-nav">
			<ul class="nav navbar-nav">
				<li><a href="#">Pricing</a></li>
				<li><a href="#">Features</a></li>
				<li><a href="<c:url value="/signup" />">Sign Up</a></li>
				<li>
					<!-- <a href="#">Login</a> -->
				</li>
				<li><a href="<c:url value="/login" />">Login</a></li>

			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
</nav>