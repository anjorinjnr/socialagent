
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="container">


	<h1>
		<span class="category">Login</span>
		<hr />
	</h1>

	<form action="<c:url value="/login" /> " method="post">
	<!-- 	<div class="row">
			<div class="col-sm-4 form-group">
			<input
					class="form-control input-lg" type="text" id="companyName"
					name="companyName" value="" placeholder="email">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 form-group">
				<input
					class="form-control input-lg" type="password" id="fullName"
					name="fullName" value="" placeholder="password">
			</div>
		</div> -->
		<div class="row">
			<div class="col-sm-4 form-group">
				<label for="email">Email</label> <input
					class="form-control input-lg" type="email" id="email" name="email"
					value="" placeholder="">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 form-group">
				<label for="email">Password</label> <input
					class="form-control input-lg" type="password" id="password" name="password"
					value="" placeholder="">
			</div>
		</div>
	
		<button class="btn btn-grove-one btn-lg btn-bold" type="submit">Login</button>
		<div class="clearfix"></div>
	</form>
</div>
