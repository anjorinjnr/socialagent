package com.socialagent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialagent.data.UserDao;
import com.socialagent.domain.User;
import com.socialagent.exception.InvalidLoginException;

@Service
public class UserServiceImpl implements UserService{

	private UserDao dao;
	
	@Autowired
	public UserServiceImpl(UserDao dao){
		this.dao = dao;
	}
	
	@Override
	public boolean signup(User user) {
		return dao.save(user);
		
	}

	@Override
	public User login(String username, String password)
			throws InvalidLoginException {
			return dao.getUserByEmail(username);
	}

}
