package com.socialagent.domain;

import java.io.Serializable;

import javax.persistence.Id;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

//import org.springframework.social.facebook.api.Page;

@JsonIgnoreProperties({ "perms"})
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class FacebookPage implements DomainObject, Serializable{

	private String access_token;
	private String category;
	@Id
	private String id;
	private String name;

	

	public FacebookPage() {

	}

	public String getAccess_token() {
		return access_token;
	}


	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "FacebookPage [access_token=" + access_token + ", category="
				+ category + ", id=" + id + ", name=" + name + "]";
	}


	
	
	
}
