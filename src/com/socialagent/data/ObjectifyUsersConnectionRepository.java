package com.socialagent.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UsersConnectionRepository;

import com.socialagent.domain.SocialChannel;

public class ObjectifyUsersConnectionRepository implements
		UsersConnectionRepository {

	private final SocialChannelDao socialChannelDao;

	private final ConnectionFactoryLocator configurationFactoryLocator;

	private final TextEncryptor textEncryptor;

	private ConnectionSignUp connectionSignUp;

	public ObjectifyUsersConnectionRepository(
			SocialChannelDao socialChannelDao,
			ConnectionFactoryLocator configurationFactoryLocator,
			TextEncryptor textEncryptor) {
		this.socialChannelDao = socialChannelDao;
		this.configurationFactoryLocator = configurationFactoryLocator;
		this.textEncryptor = textEncryptor;
	}

	/**
	 * The command to execute to create a new local user profile in the event no
	 * user id could be mapped to a connection. Allows for implicitly creating a
	 * user profile from connection data during a provider sign-in attempt.
	 * Defaults to null, indicating explicit sign-up will be required to
	 * complete the provider sign-in attempt.
	 * 
	 * @see #findUserIdsWithConnection(Connection)
	 */
	public void setConnectionSignUp(ConnectionSignUp connectionSignUp) {
		this.connectionSignUp = connectionSignUp;
	}

	public List<String> findUserIdsWithConnection(Connection<?> connection) {
		ConnectionKey key = connection.getKey();
		List<SocialChannel> userSocialConnectionList = this.socialChannelDao
				.findByProviderIdAndProviderUserId(key.getProviderId(),
						key.getProviderUserId());
		List<String> localUserIds = new ArrayList<String>();
		for (SocialChannel socialAgent : userSocialConnectionList) {
			localUserIds.add(socialAgent.getUserId());
		}

		if (localUserIds.size() == 0 && connectionSignUp != null) {
			String newUserId = connectionSignUp.execute(connection);
			if (newUserId != null) {
				createConnectionRepository(newUserId).addConnection(connection);
				return Arrays.asList(newUserId);
			}
		}
		return localUserIds;
	}

	public Set<String> findUserIdsConnectedTo(String providerId,
			Set<String> providerUserIds) {
		final Set<String> localUserIds = new HashSet<String>();

		List<SocialChannel> userSocialConnectionList = this.socialChannelDao
				.findByProviderIdAndProviderUserIds(providerId,
						providerUserIds);
		for (SocialChannel userSocialConnection : userSocialConnectionList) {
			localUserIds.add(userSocialConnection.getUserId());
		}
		return localUserIds;
	}

	public ConnectionRepository createConnectionRepository(String userId) {
	        if (userId == null) {
	            throw new IllegalArgumentException("userId cannot be null");
	        }
	        return new ObjectifyConnectionRepository(userId, socialChannelDao, configurationFactoryLocator, textEncryptor);
	    }
}
