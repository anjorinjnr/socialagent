package com.socialagent.domain;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
@JsonIgnoreProperties({ "paging" })
public class PageList {
	
	private List<FacebookPage> data;

	public List<FacebookPage> getData() {
		return data;
	}

	public void setData(List<FacebookPage> data) {
		this.data = data;
	}

}