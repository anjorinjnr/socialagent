socialagent
===========
Experimental service to use social media channels (Facebook, Twitter) and SMS and Voice to deliver automated customer service interactions.

e.g Check delivery status by sending FB message, or transfer money or process a refund.