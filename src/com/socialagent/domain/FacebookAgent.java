package com.socialagent.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Id;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.googlecode.objectify.annotation.Entity;



@Entity
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class FacebookAgent extends Agent implements DomainObject, Serializable {
	private @Id
	Long id;
	private Integer version;
	private String name;
	private String description;
	private String channel;
	private String channelId;
	@Embedded private List<Params> params;
	private String message;
	private String api;
	private String url;
	private String requestType;
	@Embedded private List<FacebookPage> channelOptions;
	private String userId;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public List<Params> getParams() {
		return params;
	}

	public void setParams(List<Params> params) {
		this.params = params;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getApi() {
		return api;
	}

	public void setApi(String api) {
		this.api = api;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public List<FacebookPage> getChannelOptions() {
		return channelOptions;
	}

	public void setChannelOptions(List<FacebookPage> channelOptions) {
		this.channelOptions = channelOptions;
	}

	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "FacebookAgent [id=" + id + ", version=" + version + ", name="
				+ name + ", desciption=" + description + ", channel=" + channel
				+ ", channelId=" + channelId + ", params=" + params
				+ ", message=" + message + ", api=" + api + ", url=" + url
				+ ", requestType=" + requestType + ", channelOptions="
				+ channelOptions + "]";
	}

	
}

  class Params {

	
	//private static final long serialVersionUID = 1L;
	public String key;
	public String value;
	
	
	@Override
	public String toString() {
		return "Params [key=" + key + ", value=" + value + "]";
	}
	
	
}
