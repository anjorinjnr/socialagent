package com.socialagent.service;

import java.util.List;

import com.socialagent.domain.Agent;
import com.socialagent.domain.FacebookAgent;

public interface AgentService {

	void addAgent(FacebookAgent agent);
	List<FacebookAgent> getAgents();
}
