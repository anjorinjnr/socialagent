<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h4>Select  Facebook Page(s)</h4> 

<c:forEach items="${pageList}" var="page">
	<ul class="list-group page">
		<li class="list-group-item">
			<h4 class="list-group-item-heading">${page.name}</h4> 
			<input	class="pull-right" type="checkbox" name="pagecheck"
			value="${page.id}">
			<p class="list-group-item-text">
				<strong>Category</strong>: ${page.category}
			</p> 
			<input type="hidden" class="access_token"
			value="${page.access_token}"> 
			<input type="hidden"
			class="pagename" value="${page.name}">


		</li>
	</ul>
</c:forEach>
