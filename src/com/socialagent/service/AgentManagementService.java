package com.socialagent.service;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import com.google.appengine.api.ThreadManager;
import com.socialagent.domain.FacebookAgent;
import com.socialagent.domain.FacebookPage;

@Service
public class AgentManagementService {

	private AgentService agentService;

	private UsersConnectionRepository usersConnectionRepository;

	private static List<FacebookAgent> agents;
	private static Map<String, ConnectionRepository> userConnection;

	private static final Logger log = Logger
			.getLogger(AgentManagementService.class.getName());

	@Autowired
	public AgentManagementService(AgentService agentService,
			UsersConnectionRepository usersConnectionRepository) {
		this.usersConnectionRepository = usersConnectionRepository;
		this.agentService = agentService;
		userConnection = new HashMap<>();
	}

	public void start() {

		if (this.agentService == null ||  this.usersConnectionRepository == null) {
			log.severe("Unable to start because AgentService and/or UsersConnectionRepository are missing");
			return;
		}
		agents = agentService.getAgents();

		/**
		 * get all created agents, then get the connection for the owner of the
		 * agent, then create a listener to listen on the agent's channels
		 * 
		 */
		for (FacebookAgent agent : agents) {

			ConnectionRepository cr = null;
			if (userConnection.containsKey(agent.getUserId())) {
				cr = userConnection.get(agent.getUserId());
			} else {
				cr = usersConnectionRepository.createConnectionRepository(agent
						.getUserId());
				userConnection.put(agent.getUserId(), cr);
			}

			if (agent.getChannel().equals("facebook")) {
				RestOperations fb = cr.getPrimaryConnection(Facebook.class)
						.getApi().restOperations();
				for (FacebookPage page : agent.getChannelOptions()) {
					Thread thread;
					try {
						thread = ThreadManager.createBackgroundThread( new FacebookListener(fb, page));
						thread.start();
					} catch (URISyntaxException e) {
						// TODO Auto-generated catch block
						log.severe(e.getMessage());
					}	
					
				}

			}
		}
	}

}
