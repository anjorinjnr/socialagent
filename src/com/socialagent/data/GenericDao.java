package com.socialagent.data;


import java.util.List;

import com.socialagent.domain.DomainObject;

/**
 * Facade Interface for data access objects
 * @param <T> 
 * @author Ebenezer Anjorin
 */
public interface GenericDao< T extends DomainObject> {

    /**
     *
     * @param id
     * @return
     */
    public T get(Long id);

    /**
     *
     * @return
     */
    public List<T> getAll();

    /**
     *
     * @param object
     */
    public boolean save(T object);

    /**
     *
     * @param object
     */
    public void delete(T object);
    
  
}
