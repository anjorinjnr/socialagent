package com.socialagent.data;

import org.springframework.stereotype.Repository;

import com.socialagent.domain.FacebookAgent;
import com.socialagent.domain.User;
import com.socialagent.exception.InvalidLoginException;

@Repository
public class FacebookAgentDaoObjectify extends GenericDaoObjectify<FacebookAgent> implements FacebookAgentDao {


	public FacebookAgentDaoObjectify() {
		super(FacebookAgent.class);
		
	}


}
