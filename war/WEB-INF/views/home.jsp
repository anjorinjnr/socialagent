
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="container">


	<div class="row">
		<ul id="homeTabs" class="nav nav-tabs">
			<li class="active"><a href="#agents" data-toggle="tab">Agents</a></li>
			<li><a href="#history" data-toggle="tab">History</a></li>
		</ul>


		<div id="tabContent" class="tab-content">
			<div class="tab-pane active" id="agents">
				<a class="btn btn-grove-one" id="addAgent" href="#"> <i
					class="glyphicons user_add"><i></i></i> Add New Agent
				</a>
			</div>
			<div class="tab-pane" id="history">History...</div>
		</div>
	</div>


</div>

<!-- New Agent Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Create New Agent</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="createAgent()">Create</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script>
	/* function manageFb(channelId) {

		var url = "<c:url value="/ajax/managepages/" />" + channelId;
		$.get(url, function(data) {
			$(".modal-body").html(data);
			$('#myModal').modal("show");
		});
	} */
	$(function() {

		$("#addAgent").click(function() {
			var url = "<c:url value="/ajax/newagent/" />"
			$.get(url, function(data) {
				$(".modal-body").html(data);
				$('#myModal').modal("show");
			});

		});

	});
</script>
