package com.socialagent.data;

import org.springframework.stereotype.Repository;

import com.socialagent.domain.FacebookPage;
import com.socialagent.domain.User;
import com.socialagent.exception.InvalidLoginException;

@Repository
public class FacebookPageDaoObjectify extends GenericDaoObjectify<FacebookPage> implements FacebookPageDao {
	
	public FacebookPageDaoObjectify() {
		super(FacebookPage.class);		
	}

}
