package com.socialagent.exception;

@SuppressWarnings("serial")
public class InvalidLoginException extends Exception {

	/**
	 * 
	 * @param no_users_found
	 */
	public InvalidLoginException(String no_users_found) {
		super(no_users_found);
	}

	/**
    *
    */
	public InvalidLoginException() {
	}
}
