﻿
# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
# On branch master
# Changes to be committed:
#   (use "git reset HEAD <file>..." to unstage)
#
#	renamed:    src/com/socialagent/data/SocialAgentDao.java -> src/com/socialagent/data/SocialChannelDao.java
#	renamed:    src/com/socialagent/data/SocialAgentDaoObjectify.java -> src/com/socialagent/data/SocialChannelDaoObjectify.java
#	renamed:    src/com/socialagent/domain/SocialAgent.java -> src/com/socialagent/domain/SocialChannel.java
#	deleted:    war/WEB-INF/classes/com/socialagent/data/SocialAgentDao.class
#	deleted:    war/WEB-INF/classes/com/socialagent/domain/SocialAgent.class
#
# Changes not staged for commit:
#   (use "git add/rm <file>..." to update what will be committed)
#   (use "git checkout -- <file>..." to discard changes in working directory)
#
#	modified:   .classpath
#	modified:   .settings/com.google.appengine.eclipse.core.prefs
#	modified:   .settings/org.eclipse.jdt.core.prefs
#	modified:   src/com/socialagent/controller/HomeController.java
#	modified:   src/com/socialagent/controller/IndexController.java
#	modified:   src/com/socialagent/controller/SettingsController.java
#	modified:   src/com/socialagent/controller/UserController.java
#	modified:   src/com/socialagent/data/GenericDaoObjectify.java
#	modified:   src/com/socialagent/data/ObjectifyConnectionRepository.java
#	modified:   src/com/socialagent/data/ObjectifyUsersConnectionRepository.java
#	modified:   src/com/socialagent/data/SocialChannelDao.java
#	modified:   src/com/socialagent/data/SocialChannelDaoObjectify.java
#	modified:   src/com/socialagent/data/UserDao.java
#	modified:   src/com/socialagent/data/UserDaoObjectify.java
#	modified:   src/com/socialagent/domain/SocialChannel.java
#	modified:   src/com/socialagent/service/ChannelService.java
#	modified:   src/com/socialagent/service/ChannelServiceImpl.java
#	modified:   src/com/socialagent/service/FacebookServiceImpl.java
#	modified:   src/com/socialagent/service/UserService.java
#	modified:   src/com/socialagent/service/UserServiceImpl.java
#	modified:   src/com/socialagent/utils/AppUtil.java
#	modified:   src/com/socialagent/web/config/SocialConfig.java
#	modified:   src/com/socialagent/web/config/WebConfig.java
#	modified:   war/WEB-INF/appengine-generated/datastore-indexes-auto.xml
#	modified:   war/WEB-INF/appengine-generated/local_db.bin
#	modified:   war/WEB-INF/appengine-web.xml
#	modified:   war/WEB-INF/assets/css/styles-blue.css
#	modified:   war/WEB-INF/classes/com/socialagent/controller/HomeController.class
#	modified:   war/WEB-INF/classes/com/socialagent/controller/IndexController.class
#	modified:   war/WEB-INF/classes/com/socialagent/controller/SettingsController.class
#	modified:   war/WEB-INF/classes/com/socialagent/controller/UserController.class
#	modified:   war/WEB-INF/classes/com/socialagent/data/GenericDaoObjectify.class
#	modified:   war/WEB-INF/classes/com/socialagent/data/ObjectifyConnectionRepository.class
#	modified:   war/WEB-INF/classes/com/socialagent/data/ObjectifyUsersConnectionRepository.class
#	deleted:    war/WEB-INF/classes/com/socialagent/data/SocialAgentDaoObjectify.class
#	modified:   war/WEB-INF/classes/com/socialagent/data/UserDao.class
#	modified:   war/WEB-INF/classes/com/socialagent/data/UserDaoObjectify.class
#	modified:   war/WEB-INF/classes/com/socialagent/service/ChannelService.class
#	modified:   war/WEB-INF/classes/com/socialagent/service/ChannelServiceImpl.class
#	modified:   war/WEB-INF/classes/com/socialagent/service/FacebookServiceImpl.class
#	modified:   war/WEB-INF/classes/com/socialagent/service/UserServiceImpl.class
#	modified:   war/WEB-INF/classes/com/socialagent/utils/AppUtil.class
#	modified:   war/WEB-INF/classes/com/socialagent/web/config/SocialConfig.class
#	modified:   war/WEB-INF/classes/com/socialagent/web/config/WebConfig.class
#	modified:   war/WEB-INF/logging.properties
#	modified:   war/WEB-INF/tiles.xml
#	modified:   war/WEB-INF/views/home-header.jsp
#	modified:   war/WEB-INF/views/home.jsp
#	modified:   war/WEB-INF/views/index-header.jsp
#	modified:   war/WEB-INF/views/index.jsp
#	modified:   war/WEB-INF/views/layout/main.jsp
#	modified:   war/WEB-INF/views/login.jsp
#	modified:   war/WEB-INF/views/settings.jsp
#	modified:   war/WEB-INF/views/signup.jsp
#
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#	src/com/socialagent/backend/
#	src/com/socialagent/controller/Test.java
#	src/com/socialagent/controller/TestAPIsController.java
#	src/com/socialagent/controller/msg.json
#	src/com/socialagent/data/FacebookAgentDao.java
#	src/com/socialagent/data/FacebookAgentDaoObjectify.java
#	src/com/socialagent/data/FacebookPageDao.java
#	src/com/socialagent/data/FacebookPageDaoObjectify.java
#	src/com/socialagent/domain/Agent.java
#	src/com/socialagent/domain/Conversation.java
#	src/com/socialagent/domain/FacebookAgent.java
#	src/com/socialagent/domain/FacebookPage.java
#	src/com/socialagent/domain/PageList.java
#	src/com/socialagent/domain/Params.java
#	src/com/socialagent/domain/SocialAgentResponse.java
#	src/com/socialagent/exception/NoConnectionException.java
#	src/com/socialagent/service/AgentManagementService.java
#	src/com/socialagent/service/AgentService.java
#	src/com/socialagent/service/AgentServiceImpl.java
#	src/com/socialagent/service/FacebookListener.java
#	src/com/socialagent/service/FacebookService.java
#	war/WEB-INF/assets/img/delete.png
#	war/WEB-INF/assets/img/logos/facebook.png
#	war/WEB-INF/backends.xml
#	war/WEB-INF/classes/com/socialagent/backend/
#	war/WEB-INF/classes/com/socialagent/controller/Test.class
#	war/WEB-INF/classes/com/socialagent/controller/TestAPIsController.class
#	war/WEB-INF/classes/com/socialagent/controller/msg.json
#	war/WEB-INF/classes/com/socialagent/data/FacebookAgentDao.class
#	war/WEB-INF/classes/com/socialagent/data/FacebookAgentDaoObjectify.class
#	war/WEB-INF/classes/com/socialagent/data/FacebookPageDao.class
#	war/WEB-INF/classes/com/socialagent/data/FacebookPageDaoObjectify.class
#	war/WEB-INF/classes/com/socialagent/data/SocialChannelDao.class
#	war/WEB-INF/classes/com/socialagent/data/SocialChannelDaoObjectify.class
#	war/WEB-INF/classes/com/socialagent/domain/Agent.class
#	war/WEB-INF/classes/com/socialagent/domain/Conversation.class
#	war/WEB-INF/classes/com/socialagent/domain/FacebookAgent.class
#	war/WEB-INF/classes/com/socialagent/domain/FacebookPage.class
#	war/WEB-INF/classes/com/socialagent/domain/Message.class
#	war/WEB-INF/classes/com/socialagent/domain/Messages.class
#	war/WEB-INF/classes/com/socialagent/domain/PageList.class
#	war/WEB-INF/classes/com/socialagent/domain/Params.class
#	war/WEB-INF/classes/com/socialagent/domain/SocialAgentResponse.class
#	war/WEB-INF/classes/com/socialagent/domain/SocialChannel.class
#	war/WEB-INF/classes/com/socialagent/exception/NoConnectionException.class
#	war/WEB-INF/classes/com/socialagent/service/AgentManagementService.class
#	war/WEB-INF/classes/com/socialagent/service/AgentService.class
#	war/WEB-INF/classes/com/socialagent/service/AgentServiceImpl.class
#	war/WEB-INF/classes/com/socialagent/service/FacebookListener.class
#	war/WEB-INF/classes/com/socialagent/service/FacebookService.class
#	war/WEB-INF/lib/US_export_policy.jar
#	war/WEB-INF/lib/apache-mime4j-0.6.jar
#	war/WEB-INF/lib/commons-codec-1.3.jar
#	war/WEB-INF/lib/commons-logging-1.1.1.jar
#	war/WEB-INF/lib/httpclient-4.0.jar
#	war/WEB-INF/lib/httpcore-4.0.1.jar
#	war/WEB-INF/lib/httpmime-4.0.jar
#	war/WEB-INF/lib/jackson-core-asl-1.8.5.jar
#	war/WEB-INF/lib/jackson-jaxrs-1.8.5.jar
#	war/WEB-INF/lib/jackson-mapper-asl-1.8.5.jar
#	war/WEB-INF/lib/local_policy.jar
#	war/WEB-INF/lib/objectify-4.0rc2.jar
#	war/WEB-INF/lib/spring-security-config-3.1.1.RELEASE.jar
#	war/WEB-INF/lib/spring-security-core-3.1.1.RELEASE.jar
#	war/WEB-INF/lib/spring-security-taglibs-3.1.1.RELEASE.jar
#	war/WEB-INF/views/ajax/
#	war/WEB-INF/views/connect/status.jsp

