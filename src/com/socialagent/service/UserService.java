package com.socialagent.service;

import com.socialagent.domain.User;
import com.socialagent.exception.InvalidLoginException;

public interface UserService {

	boolean signup(User user);

	User login(String username, String password) throws InvalidLoginException;
	

}
