package com.socialagent.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.DuplicateConnectionException;
import org.springframework.social.connect.NoSuchConnectionException;
import org.springframework.social.connect.NotConnectedException;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.googlecode.objectify.Key;
import com.socialagent.domain.SocialChannel;
import com.socialagent.domain.User;
import com.socialagent.exception.NoConnectionException;
import com.socialagent.utils.AppUtil;

public class ObjectifyConnectionRepository implements ConnectionRepository {

	private String userId;
	private SocialChannelDao socialChannelDao;
	private ConnectionFactoryLocator connectionFactoryLocator;
	private TextEncryptor textEncryptor;

	private static final Logger log = Logger
			.getLogger(ObjectifyConnectionRepository.class.getName());

	public ObjectifyConnectionRepository(String userId,
			SocialChannelDao socialChannelDao,
			ConnectionFactoryLocator connectionFactoryLocator,
			TextEncryptor textEncryptor) {
		this.userId = userId;
		this.socialChannelDao = socialChannelDao;
		this.connectionFactoryLocator = connectionFactoryLocator;
		this.textEncryptor = textEncryptor;
	}

	@Override
	public void addConnection(Connection<?> connection) {
		//
		// List<SocialAgent> storedConnections = this.socialChannelDao
		// .findByProviderIdAndProviderUserId(connection.getKey()
		// .getProviderId(), connection.getKey()
		// .getProviderUserId());
		// if (storedConnections.size() > 0) {
		// // not allow one providerId connect to multiple userId
		// throw new DuplicateConnectionException(connection.getKey());
		// }
		// log.info("Trying to save to connection");

		SocialChannel agentConnection = this.socialChannelDao
				.findByUserIdAndProviderIdAndProviderUserId(userId, connection
						.getKey().getProviderId(), connection.getKey()
						.getProviderUserId());

		if (agentConnection == null) {
			ConnectionData data = connection.createData();

//			agentConnection = new SocialChannel(new Key<User>(User.class, 12), userId, data.getProviderId(),
			agentConnection = new SocialChannel(userId, data.getProviderId(),
			data.getProviderUserId(), 0, data.getDisplayName(),
					data.getProfileUrl(), data.getImageUrl(),
					encrypt(data.getAccessToken()), encrypt(data.getSecret()),
					encrypt(data.getRefreshToken()), data.getExpireTime(),
					new Date());
			socialChannelDao.save(agentConnection);
		} else {
			throw new DuplicateConnectionException(connection.getKey());
		}

	}

	@Override
	public MultiValueMap<String, Connection<?>> findAllConnections() {

		List<SocialChannel> socialAgentConnections = socialChannelDao
				.findByUserId(userId);
		MultiValueMap<String, Connection<?>> connections = new LinkedMultiValueMap<String, Connection<?>>();

		Set<String> registeredProvidersIds = connectionFactoryLocator
				.registeredProviderIds();
		for (String providerId : registeredProvidersIds) {
			connections
					.put(providerId, Collections.<Connection<?>> emptyList());
		}
		for (SocialChannel agentConnection : socialAgentConnections) {
			String providerId = agentConnection.getProviderId();
			if (connections.get(providerId).size() == 0)
				connections.put(providerId, new LinkedList<Connection<?>>());

			connections.add(providerId, buildConnection(agentConnection));
		}
		return connections;
	}

	@Override
	public List<Connection<?>> findConnections(String providerId) {
		List<Connection<?>> resultList = new LinkedList<Connection<?>>();
		List<SocialChannel> agentConnectionList = this.socialChannelDao
				.findByUserIdAndProviderId(userId, providerId);
		for (SocialChannel agentConnection : agentConnectionList) {
			resultList.add(buildConnection(agentConnection));
		}
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <A> List<Connection<A>> findConnections(Class<A> apiType) {
		List<?> connections = findConnections(getProviderId(apiType));
		return (List<Connection<A>>) connections;
	}

	@Override
	public MultiValueMap<String, Connection<?>> findConnectionsToUsers(
			MultiValueMap<String, String> providerUsers) {
		if (providerUsers == null || providerUsers.isEmpty()) {
			throw new IllegalArgumentException(
					"Unable to execute find: no providerUsers provided");
		}

		MultiValueMap<String, Connection<?>> connectionsForUsers = new LinkedMultiValueMap<String, Connection<?>>();

		for (Iterator<Entry<String, List<String>>> it = providerUsers
				.entrySet().iterator(); it.hasNext();) {
			Entry<String, List<String>> entry = it.next();
			String providerId = entry.getKey();
			List<String> providerUserIds = entry.getValue();
			List<SocialChannel> userSocialConnections = this.socialChannelDao
					.findByProviderIdAndProviderUserIds(providerId,
							providerUserIds);
			List<Connection<?>> connections = new ArrayList<Connection<?>>(
					providerUserIds.size());
			for (int i = 0; i < providerUserIds.size(); i++) {
				connections.add(null);
			}
			connectionsForUsers.put(providerId, connections);

			for (SocialChannel userSocialConnection : userSocialConnections) {
				String providerUserId = userSocialConnection
						.getProviderUserId();
				int connectionIndex = providerUserIds.indexOf(providerUserId);
				connections.set(connectionIndex,
						buildConnection(userSocialConnection));
			}

		}
		return connectionsForUsers;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <A> Connection<A> findPrimaryConnection(Class<A> apiType) {
		String providerId = getProviderId(apiType);
		return (Connection<A>) findPrimaryConnection(providerId);
	}

	@Override
	public Connection<?> getConnection(ConnectionKey connectionKey) {
		SocialChannel userSocialConnection = this.socialChannelDao
				.findByUserIdAndProviderIdAndProviderUserId(userId,
						connectionKey.getProviderId(),
						connectionKey.getProviderUserId());
		if (userSocialConnection != null) {
			return buildConnection(userSocialConnection);
		}
		throw new NoSuchConnectionException(connectionKey);
	}

	@SuppressWarnings("unchecked")
	public <A> Connection<A> getConnection(Class<A> apiType,
			String providerUserId) {
		String providerId = getProviderId(apiType);
		return (Connection<A>) getConnection(new ConnectionKey(providerId,
				providerUserId));
	}

	@SuppressWarnings("unchecked")
	public <A> Connection<A> getPrimaryConnection(Class<A> apiType) {
		String providerId = getProviderId(apiType);
		Connection<A> connection = (Connection<A>) findPrimaryConnection(providerId);
		if (connection == null) {
			throw new NotConnectedException(providerId);
		}
		return connection;
	}

	@Override
	public void removeConnection(ConnectionKey connectionKey) {
		SocialChannel socialAgentConnection = this.socialChannelDao
				.findByUserIdAndProviderIdAndProviderUserId(userId,
						connectionKey.getProviderId(),
						connectionKey.getProviderUserId());
		this.socialChannelDao.delete(socialAgentConnection);
	}

	@Override
	public void removeConnections(String providerId) {
		List<SocialChannel> socialAgentConnectionList = this.socialChannelDao
				.findByUserIdAndProviderId(userId, providerId);
		for (SocialChannel socialAgentConnection : socialAgentConnectionList) {
			this.socialChannelDao.delete(socialAgentConnection);
		}
	}

	@Override
	public void updateConnection(Connection<?> connection) {
		ConnectionData data = connection.createData();
		SocialChannel socialAgentConnection = this.socialChannelDao
				.findByUserIdAndProviderIdAndProviderUserId(userId, connection
						.getKey().getProviderId(), connection.getKey()
						.getProviderUserId());
		if (socialAgentConnection != null) {
			socialAgentConnection.setDisplayName(data.getDisplayName());
			socialAgentConnection.setProfileUrl(data.getProfileUrl());
			socialAgentConnection.setImageUrl(data.getImageUrl());
			socialAgentConnection
					.setAccessToken(encrypt(data.getAccessToken()));
			socialAgentConnection.setSecret(encrypt(data.getSecret()));
			socialAgentConnection.setRefreshToken(encrypt(data
					.getRefreshToken()));
			socialAgentConnection.setExpireTime(data.getExpireTime());
			this.socialChannelDao.save(socialAgentConnection);
		}

	}

	/** helpers **/
	private Connection<?> buildConnection(SocialChannel agentConnection) {
		ConnectionData connectionData = new ConnectionData(
				agentConnection.getProviderId(),
				agentConnection.getProviderUserId(),
				agentConnection.getDisplayName(),
				agentConnection.getProfileUrl(), agentConnection.getImageUrl(),
				decrypt(agentConnection.getAccessToken()),
				decrypt(agentConnection.getSecret()),
				decrypt(agentConnection.getRefreshToken()),
				agentConnection.getExpireTime());
		ConnectionFactory<?> connectionFactory = this.connectionFactoryLocator
				.getConnectionFactory(connectionData.getProviderId());
		return connectionFactory.createConnection(connectionData);
	}

	private Connection<?> findPrimaryConnection(String providerId) {
		List<SocialChannel> socialAgentConnectionList = this.socialChannelDao
				.findByUserIdAndProviderId(userId, providerId);

		if (socialAgentConnectionList == null
				|| socialAgentConnectionList.size() < 1)
			return null;
		else
			return buildConnection(socialAgentConnectionList.get(0));

	}

	private <A> String getProviderId(Class<A> apiType) {
		return this.connectionFactoryLocator.getConnectionFactory(apiType)
				.getProviderId();
	}

	private String encrypt(String text) {
		return text != null ? textEncryptor.encrypt(text) : text;
	}

	private String decrypt(String encryptedText) {
		return encryptedText != null ? textEncryptor.decrypt(encryptedText)
				: encryptedText;
	}

	// @PostConstruct
	// public void initializeTextEncryptor() {
	// textEncryptor = Encryptors.text("!@#$QAZRFV&*567", KeyGenerators
	// .string().generateKey());
	// }
}